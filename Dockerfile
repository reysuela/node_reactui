# Extending image
FROM node:alpine

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Bundle app source
COPY . /usr/src/app

RUN npm install

# Port to listener
EXPOSE 80

# Environment variables
ENV NODE_ENV production
ENV PORT 80

# Main command
CMD [ "npm", "start" ]